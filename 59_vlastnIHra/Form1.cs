﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _59_vlastnIHra
{
    public partial class cakeRun : Form
    {
        public cakeRun()
        {
            InitializeComponent();
        }

        Image obrazek = Image.FromFile(@"resources\spriteone.png");
        Image taken = Image.FromFile(@"resources\plateEmpty.png");
        Image foodPic = Image.FromFile(@"resources\food.png");
        Image plate = Image.FromFile(@"resources\plate.png");
        Image sliceOne = Image.FromFile(@"resources\sliceOne.png");
        Image sliceTwo = Image.FromFile(@"resources\sliceTwo.png");
        Image sliceThree = Image.FromFile(@"resources\sliceThree.png");
        Image sliceFour = Image.FromFile(@"resources\sliceFour.png");
        Image sliceFive = Image.FromFile(@"resources\sliceFive.png");
        Image sliceSix = Image.FromFile(@"resources\sliceSix.png");
        Image sliceSeven = Image.FromFile(@"resources\sliceSeven.png");
        Image fullCake = Image.FromFile(@"resources\fullCake.png");
        Image victory = Image.FromFile(@"resources\uWin.png");
        Image loss = Image.FromFile(@"resources\uLose.png");
        Image mouse = Image.FromFile(@"resources\mouse.png");
        Image flowerone = Image.FromFile(@"resources\flowerone.png");
        Image flowertwo = Image.FromFile(@"resources\flowertwo.png");
        Image stonetwo = Image.FromFile(@"resources\stonetwo.png");
        Image stoneone = Image.FromFile(@"resources\stoneone.png");


        //Image gif = Image.FromFile("sprite.gif");
        Random food = new Random();
        Random mouserunX = new Random();
        Random mouserunY = new Random();

        int souradniceX = 2;
        int souradniceY = 2;

        int mouseDataX, mouseDataY;
        int mousex = 5;
        int mousey = 5;
        int x = 0;
        int y = 0;
        int sirka = 1;
        int vyska = 1;
        Pen p = new Pen(Color.Black, 1);
        int cake = 0;
        bool gameFinished = false;


        Int32 maxWidth = 0;
        Int32 maxHeight =0;
        Int32 stepsWidth = 10;
        Int32 stepsHeight = 10;

        Int32 stepWidth = 0;
        Int32 stepHeight = 0;

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

            Graphics kp = e.Graphics;
            Size p1 = panel1.Size;

            maxWidth = (Int32)p1.Width;
            maxHeight = (Int32)p1.Height;

            stepWidth = maxWidth / stepsWidth;
            stepHeight = maxHeight / stepsHeight;
            
            sirka = stepWidth;
            vyska = stepHeight;

            // vertical lines 
            for (int i = 1; i < stepsWidth; i++)
            {
                kp.DrawLine(p, (i * stepWidth), 0, (i * stepWidth), maxWidth);
            }
            // horizontal lines 
            for (int i = 1; i < stepsHeight; i++)
            {
                kp.DrawLine(p, 0, (i * stepHeight), maxHeight, (i * stepHeight));
            }
            

            kp.DrawImage(flowerone, (0 * stepWidth), (0 * stepHeight), sirka, vyska);
            kp.DrawImage(flowerone, (7 * stepWidth), (5 * stepHeight), sirka, vyska);
            kp.DrawImage(flowerone, (1 * stepWidth), (3 * stepHeight), sirka, vyska);
            kp.DrawImage(flowerone, (1 * stepWidth), (9 * stepHeight), sirka, vyska);

            kp.DrawImage(flowertwo, (6 * stepWidth), (3 * stepHeight), sirka, vyska);
            kp.DrawImage(flowertwo, (4 * stepWidth), (5 * stepHeight), sirka, vyska);

            kp.DrawImage(stonetwo, (5 * stepWidth), (0 * stepHeight), sirka, vyska);
            kp.DrawImage(stoneone, (7 * stepWidth), (8 * stepHeight), sirka, vyska);



            kp.DrawImage(foodPic, (souradniceX * stepHeight), (souradniceY * stepWidth), sirka, vyska);
            kp.DrawImage(obrazek, (x * stepHeight), (y * stepWidth), sirka, vyska);
            kp.DrawImage(mouse, (mousex * stepHeight), (mousey * stepWidth), sirka, vyska);



            if ((souradniceX) == (x))

            {

                if ((souradniceY) == (y))
                {
                    cake++;
                    timer1.Stop();
                    souradniceX = food.Next(0, 9);
                    souradniceY = food.Next(0, 9);
                    timer1.Start();
                    panel1.Refresh();
                    panel2.Refresh();
                }
            }
            if (cake == 8)
            {
                kp.DrawImage(victory, (0 * stepWidth), (0 * stepHeight), (10 * stepWidth), (10 * stepHeight));
                gameFinished = true;
                reset.Visible = true;
            }
            if (cake < 0)
            {
                mousex = (10 * stepWidth);
                mousey = (10 * stepHeight);
                mousetimer.Enabled = false;
                timer1.Enabled = false;

                kp.DrawImage(loss, (0 * stepWidth), (0 * stepHeight), (10 * stepWidth), (10 * stepHeight));
                gameFinished = true;
                //mousetimer.Enabled = false;
                reset.Visible = true;


            }

            //mouse hit

            if ((mousex) == (x))

            {

                if ((mousey) == (y))
                {
                    cake--;
                    mousetimer.Enabled = false;
                    mousex = mouserunX.Next(0, 9);
                    mousey = mouserunY.Next(0, 9);
                    mousetimer.Enabled = true;
                    panel1.Refresh();
                    panel2.Refresh();
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            souradniceX = food.Next(0, 9); 
            souradniceY = food.Next(0, 9);
            panel1.Refresh();
        }

        private void btN_Click(object sender, EventArgs e)
        {
            if (gameFinished == false)
            {
                if ((y) - (1) < (0))
                {
                    (y) -= (0);
                }
                if ((y) - (1) >= (0))
                {
                    (y) -= (1);
                }
                panel1.Refresh();
            }

        }
        private void btA_Click(object sender, EventArgs e)
        {
            if (gameFinished == false)
            {

                if (x - 1 < 0)
                {
                    x -= 0;
                }
                if (x - 1 >= 0)
                {
                    x -= 1;
                }

                panel1.Refresh();
            }
        }

        private void btS_Click(object sender, EventArgs e)
        {
            if (gameFinished == false)
            {

                if (y + 1 >= 10)
                {
                    y += 0;
                }
                if (y + 1 < 10)
                {
                    y += 1;
                }

                panel1.Refresh();
            }
        }

        private void btD_Click(object sender, EventArgs e)
        {
            if (gameFinished == false)
            {

                if (x + 1 >= 10)
                {
                    x += 0;
                }
                if (x + 1 < 10)
                {
                    x += 1;
                }

                panel1.Refresh();
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

            Graphics misc = e.Graphics;


            switch (cake)
            {
                case 0:
                    misc.DrawImage(plate, 0, 0, 500, 500);
                    break;
                case 1:
                    misc.DrawImage(sliceOne, 0, 0, 500, 500);

                    break;
                case 2:
                    misc.DrawImage(sliceTwo, 0, 0, 500, 500);

                    break;
                case 3:
                    misc.DrawImage(sliceThree, 0, 0, 500, 500);

                    break;
                case 4:
                    misc.DrawImage(sliceFour, 0, 0, 500, 500);

                    break;
                case 5:
                    misc.DrawImage(sliceFive, 0, 0, 500, 500);

                    break;
                case 6:
                    misc.DrawImage(sliceSix, 0, 0, 500, 500);

                    break;
                case 7:
                    misc.DrawImage(sliceSeven, 0, 0, 500, 500);

                    break;
                case 8:
                    misc.DrawImage(fullCake, 0, 0, 500, 500);

                    timer1.Enabled = false;
                    break;
            }
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char m_key = e.KeyChar;
            if (gameFinished == false)
            {

                switch (m_key)
                {
                    case 'W':
                        btN_Click(sender, e);
                        break;
                    case 'A':
                        btA_Click(sender, e);
                        break;
                    case 'D':
                        btD_Click(sender, e);
                        break;
                    case 'S':
                        btS_Click(sender, e);
                        break;
                    case 'w':
                        btN_Click(sender, e);
                        break;
                    case 'a':
                        btA_Click(sender, e);
                        break;
                    case 'd':
                        btD_Click(sender, e);
                        break;
                    case 's':
                        btS_Click(sender, e);
                        break;
                    case (char)Keys.Escape:
                        this.Close();
                        break;
                }
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void mousetimer_Tick(object sender, EventArgs e)
        {
            //mouse control

            if (gameFinished == false)
            {
                mouseDataX = mouserunX.Next(0, 4);
                mouseDataY = mouserunY.Next(0, 4);

                switch (mouseDataX)
                {
                    case 0:
                        //left
                        if (mousex - 1 < 0)
                        {
                            mousex -= 0;
                        }
                        if (mousex - 1 >= 0)
                        {
                            mousex -= 1;
                        }
                        panel1.Refresh();
                        break;
                    case 1:
                        //right
                        if (mousex + 1 >= 10)
                        {
                            mousex += 0;
                        }
                        if (mousex + 1 < 10)
                        {
                            mousex += 1;
                        }

                        panel1.Refresh();

                        break;
                    case 2:
                        //down
                        if (mousey + 1 >= 10)
                        {
                            mousey += 0;
                        }
                        if (mousey + 1 < 10)
                        {
                            mousey += 1;
                        }
                        panel1.Refresh();

                        break;
                    case 3:
                        //up
                        if (mousey - 1 < 0)
                        {
                            mousey -= 0;
                        }
                        if (mousey - 1 >= 0)
                        {
                            mousey -= 1;
                        }
                        panel1.Refresh();
                        break;
                }
            }
            if (gameFinished == true)
            {
                mousex = 10;
                mousey = 10;
                mousetimer.Enabled = false;
                timer1.Enabled = false;

            }

        }

        private void reset_Click(object sender, EventArgs e)
        {
            souradniceX = 2;
            souradniceY = 2;
            mousex = 5;
            mousey = 5;

            x = 0;
            y = 0;
            sirka = 1;
            vyska = 1;
            cake = 0;
            gameFinished = false;
            mousetimer.Enabled = true;
            panel1.Refresh();
            panel2.Refresh();
            reset.Visible = false;

        }
    }
}
